/****************************************************************************
 *   $Id:: i2c.c 5865 2010-12-08 21:42:21Z usb00423                         $
 *   Project: NXP LPC17xx I2C example
 *
 *   Description:
 *     This file contains I2C code example which include I2C initialization, 
 *     I2C interrupt handler, and APIs for I2C access.
 *
 ****************************************************************************
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * products. This software is supplied "AS IS" without any warranties.
 * NXP Semiconductors assumes no responsibility or liability for the
 * use of the software, conveys no license or title under any patent,
 * copyright, or mask work right to the product. NXP Semiconductors
 * reserves the right to make changes in the software without
 * notification. NXP Semiconductors also make no representation or
 * warranty that such application will be suitable for the specified
 * use without further testing or modification.
****************************************************************************/
#include "LPC17xx.h"
#include "types.h"
#include "i2c.h"
#include <string.h>

volatile uint32_t I2CMasterState = I2C_IDLE;
volatile uint32_t I2CSlaveState = I2C_IDLE;

volatile uint32_t timeout = 0;

volatile uint8_t I2CMasterBuffer[BUFSIZE];
volatile uint8_t I2CSlaveBuffer[BUFSIZE];
volatile uint32_t I2CCount = 0;
volatile uint32_t I2CReadLength;
volatile uint32_t I2CWriteLength;

volatile uint32_t RdIndex = 0;
volatile uint32_t WrIndex = 0;

/* 
From device to device, the I2C communication protocol may vary, 
in the example below, the protocol uses repeated start to read data from or 
write to the device:
For master read: the sequence is: STA,Addr(W),offset,RE-STA,Addr(r),data...STO 
for master write: the sequence is: STA,Addr(W),offset,RE-STA,Addr(w),data...STO
Thus, in state 8, the address is always WRITE. in state 10, the address could 
be READ or WRITE depending on the I2C command.
*/   

/*****************************************************************************
** Function name:		I2C_IRQHandler
**
** Descriptions:		I2C interrupt handler, deal with master mode only.
**
** parameters:			None
** Returned value:		None
** 
*****************************************************************************/
void I2C0_IRQHandler(void) 
{
	uint8_t StatValue;

	timeout = 0;
	// this handler deals with master read and master write only
	StatValue = LPC_I2C0->I2STAT;
	switch ( StatValue )
	{
		case 0x08:			/* A Start condition is issued. */
			/*
			 * A START condition has been transmitted.
			 * We now send the slave address and initialize
			 * the write buffer
			 * (we always start with a write after START+SLA)
			 */
			WrIndex = 0;
			LPC_I2C0->I2DAT = I2CMasterBuffer[WrIndex++];
			LPC_I2C0->I2CONCLR = (I2CONCLR_SIC | I2CONCLR_STAC);
			break;
	
		case 0x10:			/* A repeated started is issued */
			/*
			 * A repeated START condition has been transmitted.
			 * Now a second, read, transaction follows so we
			 * initialize the read buffer.
			 */
			RdIndex = 0;
			/* Send SLA with R bit set, */
			LPC_I2C0->I2DAT = I2CMasterBuffer[WrIndex++];
			LPC_I2C0->I2CONCLR = (I2CONCLR_SIC | I2CONCLR_STAC);
			break;
	
		case 0x18:			/* Regardless, it's a ACK */
			/*
			 * SLA+W has been transmitted; ACK has been received.
			 * We now start writing bytes.
			 */

			if ( I2CWriteLength == 1 )
			{
				LPC_I2C0->I2CONSET = I2CONSET_STO; 	// Set Stop flag
				I2CMasterState = I2C_NO_DATA;
			}
			else
			{
				LPC_I2C0->I2DAT = I2CMasterBuffer[WrIndex++];
			}
			LPC_I2C0->I2CONCLR = I2CONCLR_SIC;
			break;

		case 0x28:	/* Data byte has been transmitted, regardless ACK or NACK */
			/*
			 * Data in I2DAT has been transmitted; ACK has been received.
			 * Continue sending more bytes as long as there are bytes to send
			 * and after this check if a read transaction should follow.
			 */
			if ( WrIndex < I2CWriteLength )
			{
				/* Keep writing as long as bytes avail */
				LPC_I2C0->I2DAT = I2CMasterBuffer[WrIndex++]; /* this should be the last one */
			}
			else
			{
				if ( I2CReadLength != 0 )
				{
					// Send a Repeated START to initialize a read transaction
					// (handled in state 0x10)
					LPC_I2C0->I2CONSET = I2CONSET_STA;	/* Set Repeated-start flag */
				}
				else
				{
					LPC_I2C0->I2CONSET = I2CONSET_STO;      /* Set Stop flag */
					I2CMasterState = I2C_OK;
				}
			}
			LPC_I2C0->I2CONCLR = I2CONCLR_SIC;
			break;

		case 0x30:
			/*
			 * Data byte in I2DAT has been transmitted; NOT ACK has been received
			 * Send a STOP condition to terminate the transaction and inform the
			 * I2CEngine that the transaction failed.
			 */
			LPC_I2C0->I2CONSET = I2CONSET_STO;      /* Set Stop flag */
			LPC_I2C0->I2CONCLR = I2CONCLR_SIC;
			I2CMasterState = I2C_NACK_ON_DATA;
			break;

		case 0x40:	/* Master Receive, SLA_R has been sent */
			/*
			 * SLA+R has been transmitted; ACK has been received.
			 * Initialize a read.
			 * Since a NOT ACK is sent after reading the last byte,
			 * we need to prepare a NOT ACK in case we only read 1 byte.
			 */
			if ((RdIndex + 1) < I2CReadLength)
			{
				// Will go to State 0x50
				LPC_I2C0->I2CONSET = I2CONSET_AA;		//assert ACK after data is received
			}
			else
			{
				// Will go to State 0x58
				LPC_I2C0->I2CONCLR = I2CONCLR_AAC;	//NACK after data is received
			}
			LPC_I2C0->I2CONCLR = I2CONCLR_SIC;
			break;
			
		case 0x50:	/* Data byte has been received, regardless following ACK or NACK */
			/*
			 * Data byte has been received; ACK has been returned.
			 * Read the byte and check for more bytes to read.
			 * Send a NOT ACK after the last byte is received
			 */
			//if(I2CSlaveBuffer)
			I2CSlaveBuffer[RdIndex++] = LPC_I2C0->I2DAT;
			if ((RdIndex + 1) < I2CReadLength)
			{
				LPC_I2C0->I2CONSET = I2CONSET_AA;		// ACK after data is received
			}
			else
			{
				LPC_I2C0->I2CONCLR = I2CONCLR_AAC;	//NACK on last byte
			}
			LPC_I2C0->I2CONCLR = I2CONCLR_SIC;
			break;
	
		case 0x58:
			I2CSlaveBuffer[RdIndex++] = LPC_I2C0->I2DAT;
			I2CMasterState = I2C_OK;
			LPC_I2C0->I2CONSET = I2CONSET_STO;		// Set Stop flag
			LPC_I2C0->I2CONCLR = I2CONCLR_SIC;		// Clear SI flag
			break;

		case 0x20:								// regardless, it's a NACK
		case 0x48:
			LPC_I2C0->I2CONSET = I2CONSET_STO;      // Set Stop flag
			I2CMasterState = I2C_NACK_ON_ADDRESS;
			LPC_I2C0->I2CONCLR = I2CONCLR_SIC;
			break;

		case 0x38:								// Arbitration lost
		default:
			I2CMasterState = I2C_ARBITRATION_LOST;
			LPC_I2C0->I2CONCLR = I2CONCLR_SIC;
			break;
	}
	return;
}



/*****************************************************************************
** Function name:		I2CStart
**
** Descriptions:		Create I2C start condition, a timeout
**				value is set if the I2C never gets started,
**				and timed out. It's a fatal error. 
**
** parameters:			None
** Returned value:		true or false, return false if timed out
** 
*****************************************************************************/
uint32_t I2CStart (void)
{
	uint32_t retVal = FALSE;

	timeout = 0;
	// Issue a start condition
	LPC_I2C0->I2CONSET = I2CONSET_STA;// Set Start flag

	// Wait until START transmitted
	while (1)
	{
		if (I2CMasterState == I2C_STARTED)
		{
			retVal = TRUE;
			break;
		}
		if (timeout >= MAX_TIMEOUT)
		{
			retVal = FALSE;
			break;
		}
		timeout++;
	}
	return retVal;
}

/*****************************************************************************
** Function name:		I2CStop
**
** Descriptions:		Set the I2C stop condition, if the routine
**				never exit, it's a fatal bus error.
**
** parameters:			None
** Returned value:		true or never return
** 
*****************************************************************************/
uint32_t I2CStop ()
{
	LPC_I2C0->I2CONSET = I2CONSET_STO;  // Set Stop flag
	LPC_I2C0->I2CONCLR = I2CONCLR_SIC;  // Clear SI flag

	//--- Wait for STOP detected ---
	while (LPC_I2C0->I2CONSET & I2CONSET_STO);
	return TRUE;
}

/*****************************************************************************
** Function name:		I2CInit
**
** Descriptions:		Initialize I2C controller as a master
**
** parameters:			None
** Returned value:		None
** 
*****************************************************************************/
uint32_t I2CInit( void )
{
//	 Set up clock and power for I2C0 module
//	LPC_SC->PCONP |= (1 << 7);

	// set PIO0.27 and PIO0.28 to I2C0 SDA and SCL
	// function to 01 on both SDA and SCL.
	LPC_PINCON->PINSEL1 &= ~((0x03<<22)|(0x03<<24));
	LPC_PINCON->PINSEL1 |= ((0x01<<22)|(0x01<<24));

	//--- Clear flags ---
	LPC_I2C0->I2CONCLR = I2CONCLR_AAC | I2CONCLR_SIC | I2CONCLR_STAC | I2CONCLR_I2ENC;

	//--- Reset registers ---
//	#if FAST_MODE_PLUS
//	LPC_PINCON->I2CPADCFG |= ((0x1<<0)|(0x1<<2));
//	LPC_I2C0->SCLL   = I2SCLL_HS_SCLL;
//	LPC_I2C0->SCLH   = I2SCLH_HS_SCLH;
//	#else
	LPC_PINCON->I2CPADCFG &= ~((0x1<<0)|(0x1<<2));
	LPC_I2C0->I2SCLL   = I2SCLL_SCLL;
	LPC_I2C0->I2SCLH   = I2SCLH_SCLH;
//	#endif

	// Install interrupt handler
	NVIC_EnableIRQ(I2C0_IRQn);

	LPC_I2C0->I2CONSET = I2CONSET_I2EN;
	return TRUE;
}


/*****************************************************************************
** Function name:		I2CEngine
**
** Descriptions:		The routine to complete a I2C transaction
**						from start to stop. All the intermitten
**						steps are handled in the interrupt handler.
**						Before this routine is called, the read
**						length, write length, I2C master buffer,
**						and I2C command fields need to be filled.
**						see i2cmst.c for more details. 
**
** Returned value:		master state of current I2C port.
** 
*****************************************************************************/
uint32_t I2CEngine ()
{
	LPC_I2C0->I2CONSET = I2CONSET_STA;	// Issue a start condition

	I2CMasterState = I2C_BUSY;

	while (I2CMasterState == I2C_BUSY)
	{
		if (timeout >= MAX_TIMEOUT)
		{
			I2CMasterState = I2C_TIME_OUT;
			break;
		}
		timeout++;
	}
	LPC_I2C0->I2CONCLR = I2CONCLR_STAC;
	
	return I2CMasterState;
}

//Writes a single byte of data to an I2C device
// device = I2C write address
// reg = the device register
// data = the data value to be written
uint8_t i2c0_write_one_byte (unsigned char device, unsigned char reg, unsigned char data)
{
	char state = 0;
	I2CWriteLength = 3;			//write one byte
	I2CReadLength = 0;			//read nothing
	I2CMasterBuffer[0] = device << 1 | 0x00;	//device address
	I2CMasterBuffer[1] = reg;	//device register
	I2CMasterBuffer[2] = data;	//device register
	if ((state = I2CEngine()) != I2C_OK)
	{
		return FALSE;
	};
	return TRUE;
}

uint8_t i2c0_write_bytes (unsigned char device, unsigned char reg, uint8_t len, unsigned char *data)
{
	char state = 0;
	I2CWriteLength = len + 2;			//write one byte
	I2CReadLength = 0;			//read nothing
	I2CMasterBuffer[0] = device << 1 | 0x00;	//device address
	I2CMasterBuffer[1] = reg;	//device register
	if ((len + 2) > BUFSIZE)
		return FALSE;
	memcpy ((uint8_t *)&I2CMasterBuffer[2], data, len);

	if ((state = I2CEngine()) != I2C_OK)
	{
		return FALSE;
	};
	return TRUE;
}

//Read data from an I2C device and returns 8 bits
// device = I2C write address
// reg = the device register to be read
uint8_t i2c0_read_one_byte (unsigned char device, unsigned char reg, unsigned char *data)
{
	char state = 0;
	I2CWriteLength = 2;			//write one byte
	I2CReadLength = 1;			//read nothing
	I2CMasterBuffer[0] = device << 1 | 0x00;	//device address + write
	I2CMasterBuffer[1] = reg;					//device register
	I2CMasterBuffer[2] = device << 1 | 0x01;	//device address + read
	if ((state = I2CEngine()) != I2C_OK)
	{
		return FALSE;
	};

	*data = I2CSlaveBuffer[0];
	return TRUE;
}

//uint8_t i2c0_read_only_byte (unsigned char device, unsigned char reg, unsigned char *data)
//{
//	char state = 0;
//	I2CWriteLength = 2;			//write one byte
//	I2CReadLength = 1;			//read nothing
//	I2CMasterBuffer[0] = device << 1 | 0x00;	//device address + write
//	I2CMasterBuffer[1] = reg;
//	I2CMasterBuffer[2] = device << 1 | 0x01;	//device address + read
//	if ((state = I2CEngine()) != I2C_OK)
//	{
//		return FALSE;
//	};
//
//	*data = I2CSlaveBuffer[0];
//	return TRUE;
//}

//Read data from an I2C device and returns 8 bits
// device = I2C write address
// reg = the device register to be read
uint8_t i2c0_read_bytes (unsigned char device, unsigned char reg, uint8_t len, uint8_t *data)
{
	char state = 0;
	I2CWriteLength = 2;			//write one byte
	I2CReadLength = len;			//read nothing
	I2CMasterBuffer[0] = device << 1 | 0x00;	//device address + write
	I2CMasterBuffer[1] = reg;	//device register
	I2CMasterBuffer[2] = device << 1 | 0x01;	//device address + read
	if ((state = I2CEngine()) != I2C_OK)
	{
		return FALSE;
	};

	memcpy (data, (uint8_t *)I2CSlaveBuffer, len);
	return TRUE;
}

/******************************************************************************
**                            End Of File
******************************************************************************/

