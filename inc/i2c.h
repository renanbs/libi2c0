/****************************************************************************
 *   $Id:: i2c.h 5865 2010-12-08 21:42:21Z usb00423                         $
 *   Project: NXP LPC17xx I2C example
 *
 *   Description:
 *     This file contains I2C code header definition.
 *
 ****************************************************************************
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * products. This software is supplied "AS IS" without any warranties.
 * NXP Semiconductors assumes no responsibility or liability for the
 * use of the software, conveys no license or title under any patent,
 * copyright, or mask work right to the product. NXP Semiconductors
 * reserves the right to make changes in the software without
 * notification. NXP Semiconductors also make no representation or
 * warranty that such application will be suitable for the specified
 * use without further testing or modification.
****************************************************************************/
#ifndef __I2C_H 
#define __I2C_H

/*
 * These are states returned by the I2CEngine:
 *
 * IDLE     - is never returned but only used internally
 * PENDING  - is never returned but only used internally in the I2C functions
 * ACK      - The transaction finished and the slave returned ACK (on all bytes)
 * NACK     - The transaction is aborted since the slave returned a NACK
 * SLA_NACK - The transaction is aborted since the slave returned a NACK on the SLA
 *            this can be intentional (e.g. an 24LC08 EEPROM states it is busy)
 *            or the slave is not available/accessible at all.
 * ARB_LOSS - Arbitration loss during any part of the transaction.
 *            This could only happen in a multi master system or could also
 *            identify a hardware problem in the system.
 */
#define I2CSTATE_IDLE     0x000
#define I2CSTATE_PENDING  0x001
#define I2CSTATE_ACK      0x101
#define I2CSTATE_NACK     0x102
#define I2CSTATE_SLA_NACK 0x103
#define I2CSTATE_ARB_LOSS 0x104

#define I2C_IDLE              0
#define I2C_STARTED           1
#define I2C_RESTARTED         2
#define I2C_REPEATED_START    3
#define DATA_ACK              4
#define DATA_NACK             5
#define I2C_BUSY              6
#define I2C_NO_DATA           7
#define I2C_NACK_ON_ADDRESS   8
#define I2C_NACK_ON_DATA      9
#define I2C_ARBITRATION_LOST  10
#define I2C_TIME_OUT          11
#define I2C_OK                12


//#define FAST_MODE_PLUS      0

#define BUFSIZE             64
#define MAX_TIMEOUT         0x00FFFFFF

#define I2CMASTER           0x01
#define I2CSLAVE            0x02

//#define PCF8594_ADDR        0xA0
#define READ_WRITE          0x01
#define RD_BIT              0x01

#define I2CONSET_I2EN       (0x1<<6)  // I2C Control Set Register
#define I2CONSET_AA         (0x1<<2)
#define I2CONSET_SI         (0x1<<3)
#define I2CONSET_STO        (0x1<<4)
#define I2CONSET_STA        (0x1<<5)

#define I2CONCLR_AAC        (0x1<<2)  // I2C Control clear Register
#define I2CONCLR_SIC        (0x1<<3)
#define I2CONCLR_STAC       (0x1<<5)
#define I2CONCLR_I2ENC      (0x1<<6)

#define I2DAT_I2C			0x00000000  // I2C Data Reg
#define I2ADR_I2C			0x00000000  // I2C Slave Address Reg
#define I2SCLH_SCLH			0x00000080  // I2C SCL Duty Cycle High Reg
#define I2SCLL_SCLL			0x00000080  // I2C SCL Duty Cycle Low Reg
#define I2SCLH_HS_SCLH		0x00000008  // Fast I2C SCL Duty Cycle High Reg
#define I2SCLL_HS_SCLL		0x00000008  // Fast I2C SCL Duty Cycle Low Reg


void I2C0_IRQHandler (void);
uint32_t I2CInit (void);
uint32_t I2CStart ();
uint32_t I2CStop ();
uint32_t I2CEngine ();
uint8_t i2c0_write_bytes (unsigned char device, unsigned char reg, uint8_t len, unsigned char *data);
uint8_t i2c0_write_one_byte (unsigned char device, unsigned char reg, unsigned char data);
uint8_t i2c0_read_one_byte (unsigned char device, unsigned char reg, unsigned char *data);
//uint8_t i2c0_read_only_byte (unsigned char device, unsigned char reg, unsigned char *data);
uint8_t i2c0_read_bytes (unsigned char device, unsigned char reg, uint8_t len, uint8_t * data);


#endif /* end __I2C_H */
/****************************************************************************
**                            End Of File
*****************************************************************************/
